<?php

namespace App\Controller\Backend;

use App\Entity\Producto;
use App\Form\ProductoType;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ProductoService;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\ProductoDTService;
use Qipsius\TCPDFBundle\Controller\TCPDFController;

/**
 * @Route("/admin/producto")
 */
class ProductoController extends AbstractController
{
    /**
     * @Route("/", name="admin_producto_index", methods={"GET"})
     */
    public function index(ProductoRepository $productoRepository): Response
    {
        //Se recupera al usuario
        $user = $this->getUser();

        // if(!$this->isGranted("ROLE_PRODUCTO")) {
        //     throw new \Exception("No cuentas con los permisos para acceder a este módulo");
        // }
        $this->denyAccessUnlessGranted("ROLE_PRODUCTO");
        //dd($user);

        return $this->render('backend/producto/index.html.twig', [
            //'productos' => $productoRepository->findAll(),
            'productos' => $productoRepository->recuperarTodos(false),
        ]);
    }

    /**
     * @Route("/rows/data_table", name="admin_producto_rows_data_table", methods={"GET"})
     */
    public function rowsDataTable(Request $request, ProductoDTService $productoDTService): Response
    {

        $data = $productoDTService->getData($request->query->all());

        $response = new JSONResponse($data);

        return $response;
    }

    /**
     * @Route("/new", name="admin_producto_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        //Lanza directamente la excepción
        // $this->denyAccessUnlessGranted("ROLE_PRODUCTO_NUEVO");
        if(!$this->isGranted("ROLE_PRODUCTO_NEW")) {
            $this->addFlash('error', 'No tienes permisos para crear productos.');
            return $this->redirectToRoute("admin_producto_index");
        }

        $producto = new Producto();
        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($producto);
            $entityManager->flush();

            return $this->redirectToRoute('producto_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/producto/new.html.twig', [
            'producto' => $producto,
            'form' => $form,
        ]);
    }

    /**
     * admin/producto/{id}
     * @Route("/{id}/show", name="admin_producto_show", methods={"GET"})
     */
    public function show(Producto $producto): Response
    {
        return $this->render('backend/producto/show.html.twig', [
            'producto' => $producto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_producto_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Producto $producto, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('producto_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/producto/edit.html.twig', [
            'producto' => $producto,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_producto_delete", methods={"POST"})
     */
    public function delete(Request $request, Producto $producto, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$producto->getId(), $request->request->get('_token'))) {
            $entityManager->remove($producto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('producto_index', [], Response::HTTP_SEE_OTHER);
    }

     /**
     * @Route("/email", name="admin_producto_email", methods={"GET"})
     */
    public function email(Request $request, ProductoService $productoService) {
        $contenido = $productoService->enviarEmail();
        //Tiene que devolver una respuesta
    }

    /**
     * @Route("/pdf", name="admin_producto_pdf", methods={"GET"})
     */
    public function pdf(Request $request, ProductoService $productoService, 
        TCPDFController $tcpdf) {
        
        //Tiene que devolver una respuesta
        $pdf = $tcpdf->create();
        
        $pdf = $productoService->generarPDF($pdf);

        $pdf->Output('example_001.pdf', 'I');

    }

}
