<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categoria;
use App\Repository\CategoriaRepository;
use App\Form\CategoriaType;
use Symfony\Component\HttpFoundation\Request;

class CategoriaController extends AbstractController
{
    /**
     * Retorna el html de la lista de comentarios
     * @Route("/categorias", name="categoria_index", methods="GET")
     */
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        //REcuperar el repositorio de la entidad
        $categoriaRepository = $entityManager->getRepository(Categoria::class);
        //Recuperar todos los registros de la tabla
        //Es un arreglo de objetos
        $categorias = $categoriaRepository->findAll();
        //findBy(['activo' => true]) //Recupera los que cumplan la codición
        //find(2) Recupera un solo registro por el id
        //findOneBy(['id' => 35, 'activo' => false]) //Recupera un solo registro por el id

        return $this->render('categoria/index.html.twig', [
            'categorias' => $categorias,
        ]);
    }

    /**
     * Crear categoría
     * @Route("/categorias/nuevo", name="categoria_nuevo", methods={"GET", "POST"})
     */
    public function nuevo(Request $request): Response
    {
        //Recupera el entityManager
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = new Categoria();
        $form = $this->createForm(CategoriaType::class, $categoria);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($categoria);
            $entityManager->flush();

            $this->addFlash('success', 'Categoría creada correctamente');
            //redireccionar
            return $this->redirectToRoute('categoria_editar', [
                'id' => $categoria->getId()
            ]);
        }
        

        //Agregar lógica
        return $this->render('categoria/nuevo.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Editar categoría
     * @Route("/categorias/{id}/editar", name="categoria_editar",  methods={"GET", "POST"})
     */
    public function editar(Request $request, CategoriaRepository $categoriaRepository, $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = $categoriaRepository->find($id);
        $form = $this->createForm(CategoriaType::class, $categoria);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($categoria);
            $entityManager->flush();

            $this->addFlash('success', 'Categoría actualizada');
            //redireccionar
            return $this->redirectToRoute('categoria_editar', [
                'id' => $categoria->getId()
            ]);
        }
        return $this->render('categoria/editar.html.twig', [
            'categoria' => $categoria,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Crear categoría
     * @Route("/categorias/{id}/nuevo", name="categoria_eliminar", methods="GET")
     */
    public function eliminar(CategoriaRepository $categoriaRepository, $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = $categoriaRepository->find($id);
        //Eliminar de la base de la tabla de la bd
        $entityManager->remove($categoria);

        $entityManager->flush();

        //Regresar al index
        return $this->redirectToRoute('categoria_index');
    }
}
