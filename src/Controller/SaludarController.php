<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SaludarController extends AbstractController
{
    /**
     * @Route("/saludar/{nombre}", name="saludar")
     */
    public function saludar($nombre): Response
    {
        $nombreMayus = \strtoupper($nombre);
        return $this->render('saludar/index.html.twig', [
            'nombre' => $nombreMayus,
        ]);
    }
}
