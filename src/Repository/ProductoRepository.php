<?php

namespace App\Repository;

use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Producto[]    findAll()
 * @method Producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Producto::class);
    }

    public function recuperarTodos($activo)
    {
        //Recupera el query builder para armar las consultas
        $qb = $this->createQueryBuilder('pdo')
            ->orderBy('pdo.id', 'ASC')
            //->innerJoin('categoria', 'cta', 'WITH', 'pdo.categoria = cta.id')
            //inner join categoria as cta on (cta.id = pdo.categoria_id)
            ->innerJoin('pdo.categoria', 'cta')
            ->addSelect('pdo, cta')
            ->andWhere('pdo.activo = :activo')
            ->setParameter('activo', $activo);
            
        ;
        //Recupera el DQL de la consulta
        //dd($qb->getDQL());
        //Recupera el SQL a ejecutar
        //dd($qb->getQuery()->getSQL());

        //Devolver el resultado con los diferentes métodos que nos proporciona Doctrine
        return $qb->getQuery()->getResult();

    }

    // /**
    //  * @return Producto[] Returns an array of Producto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Producto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
