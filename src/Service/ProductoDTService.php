<?php

namespace App\Service;

use App\Utils\BaseDT;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;


class ProductoDTService
{
    private $em;
    private $router;
    private $security;

    public function __construct(EntityManagerInterface $em, RouterInterface $router, Security $security)
    {
        $this->em = $em;
        $this->router = $router;
        $this->security = $security;

    }

    public function getData( $request)
    {
        $columns = array(
            array( 'db' => 'id', 'dt' => 0 ),
            array( 'db' => 'nombre',  'dt' => 1 ),
            array( 'db' => 'descripcion',  'dt' => 2 ),
        );


        $connection = $this->em->getConnection();

        $data = $this->simple($request, $connection, 'producto', 'id', $columns);


        return $data;

    }

    private function simple ( $request, $conn, $table, $primaryKey, $columns )
    {
        $bindings = array();
        //$db = BaseDT::db( $conn );
        $db=$conn;

        // Build the SQL query string from the request
        $limit = BaseDT::limit( $request, $columns );
        $order = BaseDT::order( $request, $columns );
        $where = BaseDT::filter( $request, $columns, $bindings );

        $where = $this->addCustomFilters($where);

        // Main query to actually get the data
        $data = BaseDT::sql_exec( $db, $bindings,
            "SELECT id, ".implode(", ", BaseDT::pluck($columns, 'db')).
            " FROM $table $where $order $limit"
        );

        // Data set length after filtering
        $resFilterLength = BaseDT::sql_exec( $db, $bindings,
            "SELECT COUNT({$primaryKey}) FROM   $table $where"
        );
        
        $recordsFiltered = $resFilterLength[0]['count'];

        // Total data set length
        $resTotalLength = BaseDT::sql_exec( $db,
            "SELECT COUNT({$primaryKey}) FROM   $table $where"
        );
        $recordsTotal = $resTotalLength[0]['count'];

        /*
         * Output
         */
        return array(
            "draw"            => isset ( $request['draw'] ) ?
                intval( $request['draw'] ) :
                0,
            "recordsTotal"    => intval( $recordsTotal ),
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => $this->data_output( $columns, $data )
        );
    }

    private function addCustomFilters($where)
    {
        /*
         * Example
        $condition = ' id < 30';
        $where = $this->addCondition($where, $condition);
        */
        if ( $where === 'WHERE ' ) {
            $where = '';
        }

        return $where;
    }

    public function addCondition($where, $condicion) {
        if($where === '') {
            $where = $where.' WHERE '.$condicion;
        } else {
            $where = $where.' AND '.$condicion;
        }

        return $where;
    }


    private function data_output ( $columns, $data )
    {
        $out = array();

        for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
            $row = array();

            for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
                $column = $columns[$j];

                // Is there a formatter?
                if ( isset( $column['formatter'] ) ) {
                    if(empty($column['db'])){
                        $row[ $column['dt'] ] = $column['formatter']( $data[$i] );
                    }
                    else{
                        $row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
                    }
                }
                else {
                    if(!empty($column['db'])){
                        $row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
                    }
                    else{
                        $row[ $column['dt'] ] = "";
                    }
                }
            }
            $urlShow = $this->router->generate('admin_producto_show', ['id' => $data[$i]['id']]);
            $urlEdit = $this->router->generate('admin_producto_edit', ['id' => $data[$i]['id']]);
            $btnShow = '<a class="btn-show-item btn btn-info btn-sm" title="Consultar" href="'.$urlShow.'">Show</a>';
            $btnEdit = '<a class="btn-edit-item btn btn-primary btn-sm" title="Editar" href="'.$urlEdit.'">Edit</a>';
            $row[] = $btnEdit.' '.$btnShow;
            $out[] = $row;
        }

        return $out;
    }

}