<?php

namespace App\Utils;

class Catalogs
{
    const genderOptions = ["Hombre" => "H", "Mujer" => "M"];
    const nationalityOptions = ["Mexicana" => "M", "Extranjero" => "E"];
    const maritalStatusOptions = ["Soltero" => 1, "Casado" => 2];
    const formaRecepcionOptions = ["Web" => 1, "Oficinas" => 2];
}