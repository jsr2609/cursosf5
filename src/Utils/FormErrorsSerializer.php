<?php

namespace App\Utils;

use Symfony\Component\Form\Form;

class FormErrorsSerializer {

    private $form;

    public function __construct(Form $form)
    {
        $this->form =  $form;
    }

    public function serializeFormErrors(\Symfony\Component\Form\Form $form, $flat_array = false, $add_form_name = false, $glue_keys = '_')
    {
        $errors = array();
        $errors['global'] = array();
        $errors['fields'] = array();

        foreach ($form->getErrors() as $error) {

            $errors['global'][] = $error->getMessage();
        }

        $errors['fields'] = $this->serialize($form);

        $errors['fields'] = $this->arrayFlatten($errors['fields'], $glue_keys, (($add_form_name) ? $form->getName() : ''));

        return $errors;
    }

    public function getErrorsAsString(\Symfony\Component\Form\Form $form, $flat_array = false, $add_form_name = false, $glue_keys = '_')
    {
        $errors = $this->serializeFormErrors($form, $flat_array, $add_form_name, $glue_keys);
        dd($errors);

        $errorsString = "";
        foreach ($errors['global'] as $key => $errorMessage) {
            $errorsString = "<b>Error: </b>".$errorMessage."<br/>";
        }
        foreach ($errors['fields'] as $key => $errorMessage) {
            $errorsString = "<b>".$key.":</b> ".$errorMessage."<br/>";
        }

        return $errorsString;


    }

    private function serialize(\Symfony\Component\Form\Form $form)
    {
        $formName = $this->form->getName();
        $local_errors = array();
        foreach ($form->getIterator() as $key => $child) {

            foreach ($child->getErrors() as $error){
//                $local_errors[] = [
//                    'field' => $formName.'_'.$key,
//                    'message' => $error->getMessage(),
//                ];
                $local_errors[$key] = $error->getMessage();
            }

            if (count($child->getIterator()) > 0 && ($child instanceof \Symfony\Component\Form\Form)) {
                //dd($child);
                $local_errors[$key] = $this->serialize($child);
            }
        }

        return $local_errors;
    }

    private function arrayFlatten($array, $separator = "_", $flattened_key = '') {
        $flattenedArray = array();
        foreach ($array as $key => $value) {

            if(is_array($value)) {

                $flattenedArray = array_merge($flattenedArray,
                    $this->arrayFlatten($value, $separator,
                        (strlen($flattened_key) > 0 ? $flattened_key . $separator : "") . $key)
                );

            } else {
                $flattenedArray[] = [
                    'field' => (strlen($flattened_key) > 0 ? $flattened_key . $separator : "") . $key,
                    'message' => $value,
                ];
                //$flattenedArray[(strlen($flattened_key) > 0 ? $flattened_key . $separator : "") . $key] = $value;
            }
        }
        return $flattenedArray;
    }

}