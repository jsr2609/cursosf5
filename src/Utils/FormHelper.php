<?php

namespace App\Utils;

use Symfony\Component\Form\Form;

/**
 * Class FormHelper
 *
 * @package AppBundle\Utils
 */
class FormHelper
{
    /**
     * Recursive helper who return all found errors in a form
     *
     * @param Form $form
     *
     * @return array
     */
    public static function getFormErrors(Form $form)
    {
        $errors = [];

        // find errors of this element
        foreach ($form->getErrors() as $error) {
            $errors[] = $error;
        }

        // iterate over errors of all children
        foreach ($form->all() as $key => $child) {
            if ($child instanceof Form) {
                /**
                 * @var Form $child
                 */
                $err = self::getFormErrors($child);
                if (count($err) > 0) {
                    $errors = array_merge($errors, $err);
                }
            }
        }

        return $errors;
    }
}